from django.contrib import admin
from . import models


class HasAnswerFilter(admin.SimpleListFilter):

    title = 'présence de réponse'
    parameter_name = 'has_answer'

    def lookups(self, request, model_admin):
        return (
            ('yes', 'oui'),
            ('no', 'non'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(answer__isnull=False)
        elif self.value() == 'no':
            return queryset.filter(answer__isnull=True)
        else:
            return queryset


class AnswerInline(admin.StackedInline):

    model = models.Answer
    fields = ('text', )


class QuestionAdmin(admin.ModelAdmin):

    list_display = ('title', 'publish_date', 'has_answer')
    list_filter = ('publish_date', HasAnswerFilter, 'answer__author')
    search_fields = ('title', 'text', 'answer__text')
    inlines = (AnswerInline, )

    def has_answer(self, obj):
        try:
            obj.answer
            return True
        except models.Question.answer.RelatedObjectDoesNotExist:
            return False
    has_answer.short_description = 'a une réponse'
    has_answer.boolean = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('answer', 'answer__author')
        return qs

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.author = request.user
            instance.save()
        formset.save_m2m()


admin.site.register(models.Question, QuestionAdmin)
# admin.site.register(models.Answer)
