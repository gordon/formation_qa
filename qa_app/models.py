from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Question(models.Model):
    title = models.CharField('titre', max_length=150)
    text = models.TextField('texte')
    publish_date = models.DateTimeField('date de publication', auto_now_add=True)
    image = models.ImageField('image', upload_to='question_images', null=True)

    def __str__(self):
        return self.title


class Answer(models.Model):
    # question = models.ForeignKey(Question, verbose_name='question', on_delete=models.CASCADE, unique=True)
    question = models.OneToOneField(Question, verbose_name='question', on_delete=models.CASCADE)
    author = models.ForeignKey(User, verbose_name='auteur', on_delete=models.PROTECT)
    text = models.TextField('texte')
    publish_date = models.DateTimeField('date de publication', auto_now_add=True)

    def __str__(self):
        return f'Réponse de {self.author}'