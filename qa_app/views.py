from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse
from django.core.paginator import Paginator
from django.conf import settings
from django.db.models import Q

from .models import Question, Answer
from . import forms


def homepage(request):
    questions = Question.objects.filter(answer__isnull=False).order_by('-publish_date')\
        .select_related('answer', 'answer__author')
    search = request.GET.get('search', '')
    if search:
        questions = questions.filter(Q(title__contains=search)
                                     | Q(text__contains=search)
                                     | Q(answer__text__contains=search))
    pages = Paginator(questions, settings.QUESTIONS_PER_PAGE)
    page_number = request.GET.get('page', 1)
    page = pages.get_page(page_number)
    if request.user.is_authenticated and request.user.has_perm('qa_app.add_question'):
        if request.method == 'POST':
            form = forms.QuestionForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Votre question a bien été posée, et sera publiée lorsqu’une réponse lui sera apportée.')
                return redirect(reverse('homepage'))
        else:
            form = forms.QuestionForm()
    else:
        form = None
    return render(request, 'qa_app/homepage.html', {'questions': page, 'form': form, 'search': search})


